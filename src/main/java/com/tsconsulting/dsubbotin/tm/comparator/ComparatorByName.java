package com.tsconsulting.dsubbotin.tm.comparator;

import com.tsconsulting.dsubbotin.tm.api.entity.IHasName;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public final class ComparatorByName implements Comparator<IHasName> {

    public static final ComparatorByName INSTANCE = new ComparatorByName();

    @Override
    public int compare(@NotNull final IHasName o1, @NotNull final IHasName o2) {
        return o1.getName().compareTo(o2.getName());
    }

}
