package com.tsconsulting.dsubbotin.tm.model;

import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class User extends AbstractEntity {

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @Nullable
    private String email;

    @NotNull
    private Role role = Role.USER;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    boolean locked = false;

    @Override
    @NotNull
    public String toString() {
        return super.toString() + " : " + login + "; Role: " + role.getDisplayName() + ";";
    }

}
