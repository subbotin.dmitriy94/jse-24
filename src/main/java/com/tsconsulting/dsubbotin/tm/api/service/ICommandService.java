package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ICommandService {

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name) throws AbstractException;

    @NotNull
    AbstractCommand getCommandByArg(@Nullable String arg) throws AbstractException;

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractSystemCommand> getArguments();

    void add(@NotNull AbstractCommand command);

}
