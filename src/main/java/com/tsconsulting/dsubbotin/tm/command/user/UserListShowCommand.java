package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public final class UserListShowCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-list";
    }

    @Override
    @NotNull
    public String description() {
        return "Display user list.";
    }

    @Override
    public void execute() {
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        int index = 1;
        for (@NotNull final User user : users) TerminalUtil.printMessage(index++ + ". " + user);
    }

}
