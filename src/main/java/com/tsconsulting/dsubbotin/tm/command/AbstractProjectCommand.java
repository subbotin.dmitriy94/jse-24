package com.tsconsulting.dsubbotin.tm.command;

import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(@NotNull final Project project) {
        TerminalUtil.printMessage("Id: " + project.getId() + "\n" +
                "Name: " + project.getName() + "\n" +
                "Description: " + project.getDescription() + "\n" +
                "Status: " + project.getStatus().getDisplayName() + "\n" +
                "Create date: " + project.getCreateDate() + "\n" +
                "Start date: " + project.getStartDate()
        );
    }

}
